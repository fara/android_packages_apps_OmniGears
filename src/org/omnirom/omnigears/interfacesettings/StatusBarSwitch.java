/*
 *  Copyright (C) 2021 The OmniROM Project
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
package org.omnirom.omnigears.interfacesettings;

import android.app.ActivityManager;
import android.database.ContentObserver;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.AttributeSet;

import androidx.preference.SwitchPreference;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class StatusBarSwitch extends SwitchPreference {

    private static final String ICON_BLACKLIST = "icon_blacklist";
    private Set<String> mBlacklist;
    private ContentObserver mObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            // we must fetch again before doing anything
            mBlacklist = null;
        }
    };

    public StatusBarSwitch(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public StatusBarSwitch(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StatusBarSwitch(Context context) {
        super(context, null);
    }

    @Override
    public void onAttached() {
        super.onAttached();
        getContext().getContentResolver().registerContentObserver(
                Settings.Secure.getUriFor(ICON_BLACKLIST), false, mObserver);
    }

    @Override
    public void onDetached() {
        getContext().getContentResolver().unregisterContentObserver(mObserver);
        super.onDetached();
    }

    @Override
    protected boolean persistBoolean(boolean value) {
        if (shouldPersist()) {
            if (!value) {
                // If not enabled add to blacklist.
                if (!getBlacklist().contains(getKey())) {
                    getBlacklist().add(getKey());
                    setList(getBlacklist());
                }
            } else {
                if (getBlacklist().remove(getKey())) {
                    setList(getBlacklist());
                }
            }
        }
        return true;
    }

    private Set<String> getBlacklist() {
        if (mBlacklist == null) {
            mBlacklist = new HashSet();
            ContentResolver contentResolver = getContext().getContentResolver();
            final String s = Settings.Secure.getString(contentResolver, ICON_BLACKLIST);
            if (!TextUtils.isEmpty(s)) {
                mBlacklist.addAll(Arrays.asList(s.split(",")));
            }
        }
        return mBlacklist;
    }

    private void setList(Set<String> blacklist) {
        ContentResolver contentResolver = getContext().getContentResolver();
        Settings.Secure.putString(contentResolver, ICON_BLACKLIST,
                TextUtils.join(",", blacklist));
    }

    @Override
    protected boolean getPersistedBoolean(boolean defaultReturnValue) {
        if (!shouldPersist()) {
            return defaultReturnValue;
        }
        return !getBlacklist().contains(getKey());
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
        setChecked(getPersistedBoolean((Boolean) defaultValue));
    }
}
